FROM rappdw/docker-java-python:zulu11.43-python3.7.9

WORKDIR /usr/src/app

COPY requirements.txt ./
RUN pip install -r requirements.txt

COPY . .

ENTRYPOINT ["python", "-u", "run.py"]