# WkbvReportConverter
Mit Hilfe dieses Skript kann der offizielle Kegel-Spielbericht des WKBV ausgelesen
und in eine JSON- oder CSV-Datei konvertiert werden.

Hierbei können auch mehrere Berichte gleichzeitig als ZIP-Datei übergeben werden, um
größere Auswertungen mit den Daten durchführen zu können.

## Get Started

### Docker

Einfach das Docker-Image bauen:
```
docker build -t eckii24/wkbvreportconverter:1.0.0 .
```

und dann ausführen
```
docker run --rm --name wkbv -v ./data:/usr/src/app/data eckii24/wkbvreportconverter:1.0.0 data/pdf.pdf
```

oder
```
docker run --rm --name wkbv -v ./data:/usr/src/app/data eckii24/wkbvreportconverter:1.0.0 data/archive.zip
```

### Voraussetzungen
- Python 3
- pip

### Installation
```python
pip3 install -r requirements.txt
```

### Verwendung
Siehe hierzu auch den Ordner `example`
```python
from src.WkbvReportHandler import WkbvReportHandler

filename = "pdf.pdf"

wkbv_handler = WkbvReportHandler(filename)
wkbv_handler.convert_report()
wkbv_handler.export_as_csv()
```

## Aufbau der JSON-Datei
```json
[
  {
    "Spielnummer": "20802",
    "Datum": "09.02.2019",
    "Spielbetrieb": "Klubspiel",
    "Geschlecht": "M\u00e4nner",
    "Heim-Mannschaft": "SC Hermaringen II",
    "Gast-Mannschaft": "EKC Lonsee II",
    "Ort": "89568 Hermaringen",
    "Bahnanlage": "Vereinsheim",
    "Spielbeginn": "12:00",
    "Spielende": "15:15",
    "Liga/Klasse": "Regionalliga Alb Donau",
    "Ergebnisse Heim": [
      {
        "Mannschaftspunkte": 1.0,
        "Spieler-Nummer": 1,
        "Mannschaft": "Heim",
        "Durchg\u00e4nge": [
          {
            "Durchgang": 1,
            "Volle": 84,
            "Abr\u00e4umen": 27,
            "Fehlwurf": 2,
            "Spielerpunkte": 0.0,
            "Spielername": "Merkle, Rainer",
            "Pass-Nummer": "11820",
            "Wurf": 30
          }, ... ]
      }, ... ]
  }, ...
]
```
