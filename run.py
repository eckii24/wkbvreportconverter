import sys
from src.WkbvReportHandler import WkbvReportHandler

if len(sys.argv) != 2:
    print("Usage: python example.py <filename>")
    sys.exit(1)

filename = sys.argv[1]

wkbv_handler = WkbvReportHandler(filename)
wkbv_handler.convert_report()
wkbv_handler.export_as_csv("data/report.csv")
wkbv_handler.export_as_json("data/report.json")
