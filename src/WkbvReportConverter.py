# pylint: disable=line-too-long

import tabula


class WkbvReportConverter:
    """
    Class which reads a pdf report and extracts the data to be able to further process it.
    """

    def __init__(self, current_file):
        self.current_file = current_file
        self.match_data = {}

    def get_match_data(self):
        self._add_match_meta_data()
        self._add_match_player_data()

        return self.match_data

    def _add_match_meta_data(self):
        df_meta = tabula.read_pdf(
            self.current_file,
            area=[[77, 4, 168, 590]],
            silent=True,
            pandas_options={"header": None},
            pages="1")[0]


        self.match_data = {
            "Heim-Mannschaft": WkbvReportConverter._get_team(df_meta, True),
            "Gast-Mannschaft": WkbvReportConverter._get_team(df_meta, False),

            "Spielbetrieb": WkbvReportConverter._get_value_with_x(df_meta, [0, 1]),
            "Geschlecht": WkbvReportConverter._get_value_with_x(df_meta, [2, 3]),

            "Spielnummer": WkbvReportConverter._get_multiple_columns_meta_details(df_meta, 0, "Spielnummer"),
            "Datum": WkbvReportConverter._get_multiple_columns_meta_details(df_meta, 0, "Datum"),
            "Spielbeginn": WkbvReportConverter._get_multiple_columns_meta_details(df_meta, 3, "Spielbeginn"),
            "Spielende": WkbvReportConverter._get_multiple_columns_meta_details(df_meta, 3, "Spielende"),

            "Ort": WkbvReportConverter._get_single_columns_meta_details(df_meta, 1),
            "Bahnanlage": WkbvReportConverter._get_single_columns_meta_details(df_meta, 2),
            "Liga/Klasse": WkbvReportConverter._get_single_columns_meta_details(df_meta, 4),
        }

    def _add_match_player_data(self):
        df_match = tabula.read_pdf(
            self.current_file,
            multiple_tables=True,
            silent=True,
            pages="1")

        self.match_data["Ergebnisse Heim"] = []
        self.match_data["Ergebnisse Gast"] = []

        # Ignore the first element, because it contains the incorrect detected meta data table.
        for player_number in range(1, (len(df_match) - 1)):

            df_player_corrected = WkbvReportConverter._reformat_df_player(df_match[player_number])

            relevant_rows = WkbvReportConverter._get_relevant_rows(df_player_corrected)

            second_player = df_player_corrected.loc[4, "Name,Vorname"]
            if len(df_player_corrected) == 7 and second_player != "":
                throw_index = int(second_player.split(" (")[1].split(".)")[0]) // 30
                if throw_index > 0:
                    self._add_match_player_results(df_player_corrected, player_number, relevant_rows[:throw_index], True)
                self._add_match_player_results(df_player_corrected, player_number, relevant_rows[throw_index:], True, 4)
            else:
                self._add_match_player_results(df_player_corrected, player_number, relevant_rows, True)

            second_player = df_player_corrected.loc[4, "Name,Vorname.1"]
            if len(df_player_corrected) == 7 and  second_player != "":
                throw_index = int(second_player.split(" (")[1].split(".)")[0]) // 30
                if throw_index > 0:
                    self._add_match_player_results(df_player_corrected, player_number, relevant_rows[:throw_index], False)
                self._add_match_player_results(df_player_corrected, player_number, relevant_rows[throw_index:], False, 4)
            else:
                self._add_match_player_results(df_player_corrected, player_number, relevant_rows, False)


    def _add_match_player_results(self, df_player_corrected, player_number, relevant_rows, is_home_team, player_index = 1):
        team_suffix = "" if is_home_team else ".1"

        has_played = df_player_corrected \
            .loc[relevant_rows, :] \
            .apply(lambda x: float(x[f"F{team_suffix}"]) + float(x[f"G{team_suffix}"]), axis=1) \
            .sum() > 0

        if has_played:
            team_description = "Heim" if is_home_team else "Gast"
            pass_number = df_player_corrected.loc[player_index, f"Passnr.{team_suffix}"].replace(",", ".")

            self.match_data[f"Ergebnisse {team_description}"].append({
                "Mannschaftspunkte": float(df_player_corrected[f"MP{team_suffix}"].sum().replace(",", ".")),
                "Spieler-Nummer": player_number,
                "Spielername": df_player_corrected.loc[player_index, f"Name,Vorname{team_suffix}"].split(" (")[0],
                "Pass-Nummer": float(pass_number) if pass_number != "" else pass_number,
                "Mannschaft": team_description,
                "Durchgänge": WkbvReportConverter._get_player_data(df_player_corrected, relevant_rows, team_suffix)
            })

    @staticmethod
    def _get_team(df_meta, is_home_team):
        df_teams = df_meta.loc[5, :]
        return df_teams[0] if is_home_team else df_teams[4]

    @staticmethod
    def _get_value_with_x(df_meta, indices):
        df_subtable = df_meta.loc[1:4, indices].fillna("")
        subtable_str = WkbvReportConverter._join_with_str(df_subtable)
        for row in subtable_str:
            values = row.split(" ")
            if values[1] == "X":
                return values[0]
        return ""

    @staticmethod
    def _get_single_columns_meta_details(df_meta, index):
        df_meta_details = df_meta.loc[:4, 4:].fillna("")
        df_meta_details_str = WkbvReportConverter._join_with_str(df_meta_details)
        return df_meta_details_str[index].split(":")[1].strip()

    @staticmethod
    def _get_multiple_columns_meta_details(df_meta, index, column_name):
        df_meta_details = df_meta.loc[:4, 4:].fillna("")
        df_meta_details_str = WkbvReportConverter._join_with_str(df_meta_details)[index].replace(": ", ":")
        df_meta_details_str_items = df_meta_details_str.split(" ")

        for item in df_meta_details_str_items:
            if column_name in item:
                return item.replace(f"{column_name}:", "")
        return ""

    @staticmethod
    def _join_with_str(df):
        df = df.copy()
        for col in df.columns:
            if type(col) == str and len(col.replace(", ", ",").split(" ")) > 1:
                df.loc[df[col] == "", col] = "0 0"
        return df.apply(lambda x: x.astype(str).str.cat(sep=" "), axis=1)

    @staticmethod
    def _get_player_data(df_player, relevant_rows, team_suffix):
        result = []
        for index, row in enumerate(relevant_rows, start=1):
            result.append({
                "Durchgang": index,
                "Volle": float(df_player.loc[row, f"V{team_suffix}"].replace(",", ".")),
                "Abräumen": float(df_player.loc[row, f"A{team_suffix}"].replace(",", ".")),
                "Fehlwurf": float(df_player.loc[row, f"F{team_suffix}"].replace(",", ".")),
                "Spielerpunkte": float(df_player.loc[row, f"SP{team_suffix}"].replace(",", ".")),
                "Wurf": 30
            })
        return result

    @staticmethod
    def _get_relevant_rows(df_player):
        if len(df_player) == 5:  # missing player name
            return [0, 1, 2, 3]
        if len(df_player) == 6:  # single player name
            return [0, 2, 3, 4]
        if len(df_player) == 7:  # two player names
            return [0, 2, 3, 5]

    @staticmethod
    def _reformat_df_player(df_player):

        for col in df_player.columns:
            if col.startswith("Unnamed"):
                df_player.drop(columns=col, inplace=True)

        df_player_corrected = df_player.fillna("")
        df_player_corrected["Name, Vorname"] = df_player_corrected["Name, Vorname"].apply(lambda x: x.replace(" ", "_"))
        df_player_corrected["Name, Vorname.1"] = df_player_corrected["Name, Vorname.1"].apply(lambda x: x.replace(" ", "_"))

        df_player_corrected_str = WkbvReportConverter._join_with_str(df_player_corrected)

        columns = " ".join(df_player.columns.fillna("").str.replace(", ", ",").tolist()).split(" ")
        columns = columns[:8] + [f"{col}{'.1' if not col.endswith('.1') else ''}" for col in columns[8:]]

        df_player_corrected_df = df_player_corrected_str.str.split(" ", expand=True)
        df_player_corrected_df.columns = columns
        df_player_corrected_df["Name,Vorname"] = df_player_corrected_df["Name,Vorname"].apply(lambda x: x.replace("_", " "))
        df_player_corrected_df["Name,Vorname.1"] = df_player_corrected_df["Name,Vorname.1"].apply(lambda x: x.replace("_", " "))

        return df_player_corrected_df
