import json
import os
import pandas as pd
import shutil
import zipfile
from src.WkbvReportConverter import WkbvReportConverter

class FileExtensionError(Exception):
    pass

class WkbvReportHandler:
    def __init__(self, data_input):
        """
        Klasse zur Konvertierung und zum Export von Spielberichten.
        Es werden eine einzelne Spielberichts-Datei (PDF) oder mehrere gezippte Dateien (ZIP)
        in ein pandas.DataFrame umgewandelt. Auf dieser Basis können die Daten als JSON oder CSV-Datei exportiert werden

        :param data_input: Pfad der PDF oder ZIP-Datei
        """
        if data_input[-4:] != ".pdf" and data_input[-4:] != ".zip":
            raise FileExtensionError

        self.data_input = data_input
        self.converted_data = []
        self.converted_data_as_dataframe = None

    def _convert_single_file(self, data_input):
        """
        Konvertiert eine einzelne PDF-Datei in ein JSON-Objekt.
        Methode speichert dies in der Variable 'self.converted_data' ohne die Daten zurückzugeben.

        :param data_input: Pfad der Pdf-Datei
        """
        if data_input[-4:] != ".pdf":
            raise FileExtensionError

        wkbv = WkbvReportConverter(data_input)
        self.converted_data.append(wkbv.get_match_data())

    def _convert_zipped_file(self):
        """
        Konvertiert eine der Klasse übergebene ZIP-Datei in ein JSON-Objekt.
        Methode speichert dies in der Variable 'self.converted_data' ohne die Daten zurückzugeben.
        """
        if self.data_input[-4:] != ".zip":
            raise FileExtensionError

        with zipfile.ZipFile(self.data_input, "r") as zip_ref:
            zip_ref.extractall("files")

        for file_to_convert in os.listdir("files"):
            if file_to_convert[-4:] == ".pdf":
                print(file_to_convert, flush=True)
                self._convert_single_file(os.path.join("files", file_to_convert))

        shutil.rmtree("files")

    def convert_report(self):
        """
        Konvertiert die der Klasse übergebene PDF- oder ZIP-Datei in ein JSON-Objekt.

        :return: JSON-Objekt mit den konvertierten Spielberichten
        """
        if self.data_input[-4:] == ".pdf":
            self._convert_single_file(self.data_input)
        elif self.data_input[-4:] == ".zip":
            self._convert_zipped_file()
        else:
            raise FileExtensionError

        self._save_as_dataframe()
        return self.converted_data

    @staticmethod
    def __restructure_data_to_single_line(data):
        """
        Restrukturiert die konvertierten Spielberichtsdaten von einem verschachtelten JSON-Objekt in eine
        Liste mit Objekten pro Durchgang (keine Schachtelung)

        :param data: Daten die konvertiert werden sollen
        :return: Konvertierte Daten
        """
        result_for_dataframe = []
        for match in data:
            match_keys = {}
            for match_key, match_value in match.items():
                if not type(match_value) == list:
                    match_keys[match_key] = match_value
                else:
                    for player in match_value:
                        player_keys = {}
                        for player_key, player_value in player.items():
                            if not type(player_value) == list:
                                player_keys[player_key] = player_value
                            else:
                                for part in player_value:
                                    result = {}
                                    result.update(match_keys)
                                    result.update(player_keys)
                                    result.update(part)
                                    result_for_dataframe.append(result)
        return result_for_dataframe

    def _save_as_dataframe(self):
        """
        Konvertiert die in der Klasse gespeicherten, konvertierten Daten in ein pandas.DataFrame
        und speichert dies in der Klassen-Variable 'self.converted_data_as_dataframe'
        """
        self.converted_data_as_dataframe = pd.DataFrame(
            WkbvReportHandler.__restructure_data_to_single_line(self.converted_data))

    def export_as_csv(self, filename="report.csv"):
        """
        Exportiert die konvertieren Spielberichtsdaten in eine CSV-Datei.

        :param filename: Optionaler Dateiname der CSV-Datei
        """
        self.converted_data_as_dataframe.to_csv(filename, sep=";", decimal=",", float_format='%.3f')

    def export_as_json(self, filename="report.json"):
        """
        Exportiert die konvertieren Spielberichtsdaten in eine JSON-Datei.

        :param filename: Optionaler Dateiname der JSON-Datei
        """
        with open(filename, "w") as f:
            json.dump(self.converted_data, f)
